wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
rpm --import http://pkg.jenkins-ci.org/redhat-stable/jenkins-ci.org.key
yum install -y jenkins
systemctl start jenkins.service
systemctl enable jenkins.service
firewall-cmd --zone=public --permanent --add-port=8080/tcp
firewall-cmd --reload

# Creacion de un grupo docker para a�adir el usuario jenkins. de esta forma las pipelines podran ejecutar comandos docker sin sudo
groupadd docker
usermod -aG docker jenkins
gpasswd -a jenkins docker

systemctl daemon-reload
systemctl restart docker
service jenkins restart
